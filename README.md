# Smartbanner App 

> Smartbanner App für IOS und Android. Funktioniert ohne Frameworks.

## Installation

### META Tags
```html
<meta name="apple-itunes-app" content="app-id=1436268036">
<meta name="google-play-app" content="app-id=de.sesam.app&hl=de">
...
<link rel="apple-touch-icon" href="apple-touch-icon.png">
<link rel="android-touch-icon" href="android-icon.png" />
```

### CSS
```html
<link rel="stylesheet" href="smart-app-banner.css" type="text/css" media="screen">
```

### JS
```html
<script src="smart-app-banner.js"></script>
```

## Usage 
```javascript
<script type="text/javascript">
    new SmartBanner({
        daysHidden: 15,   // days to hide banner after close button is clicked (defaults to 15)
        daysReminder: 90, // days to hide banner after "VIEW" button is clicked (defaults to 90)
        appStoreLanguage: 'de', // language code for the App Store (defaults to user's browser language)
        title: 'MySESAM',
        author: 'SESAM Homebox',
        button: 'Details',
        store: {
            ios: 'IM App Store',
            android: 'In Google Play',
            windows: 'In Windows store'
        },
        price: {
            ios: 'GRATIS',
            android: 'GRATIS',
            windows: 'GRATIS'
        }
        //, theme: '' // put platform type ('ios', 'android', etc.) here to force single theme on all device
        // , icon: 'icon.png' // full path to icon image if not using website icon image
        // , force: 'ios' // Uncomment for platform emulation
    });
```
